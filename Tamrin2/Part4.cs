﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamrin2
{
    class Part4
    {
        public Part4()
        {
            Console.WriteLine("Part 4 ");
            Console.WriteLine("Enter the first number : ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the second number : ");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("GCD is: {0} " , GCD(a, b));
            Console.WriteLine("LCM is: {0} ", LCM(a, b));

        }
        private static int GCD(int num1 , int num2)
        {
            int r;
            while (num2 != 0)
            {
                r = num1 % num2;
                num1 = num2;
                num2 = r;
            }
            return num1;
        }
        private static int LCM(int num1 , int num2)
        {
            int x = num1;
            int y = num2;
            while (num1 != num2)
            {
                if (num1 > num2)
                {
                    num1 = num1 - num2;
                }
                else
                {
                    num2 = num2 - num1;
                }
            }
            return (x * y) / num1;
        }
    }
}
