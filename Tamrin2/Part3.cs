﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamrin2
{
    class Part3
    {
        public Part3()
        {
            Console.WriteLine("Part 3 ");
            Console.WriteLine("Please enter age in days:");
            int days = Int32.Parse(Console.ReadLine());
            int year = CalcYears(days);
            int month = CalcMonth(days);
            int day = CalcDays(days);
            Console.WriteLine("years: {0} , monthes: {1} , days: {2}", year, month, day);

        }
        private static int CalcYears(int days)
        {
            return days / 365;
        }
        private static int CalcMonth(int days)
        {
            int remainingDays = days % 365;
            return remainingDays / 30;
        }
        private static int CalcDays(int days)
        {
            int remainingDays = days % 365;
            return remainingDays % 30;
        }

    }
}
