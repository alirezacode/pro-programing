﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamrin2
{
    class Part2
    {
        public Part2()
        {
            Console.WriteLine("Part 2 ");
            Console.WriteLine("Please enter a:");
            int a = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Please enter b:");
            int b = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Please enter c:");
            int c = Int32.Parse(Console.ReadLine());
            double delta = CalcDelta(a, b, c);
            Console.WriteLine("x1 is: {0}", CalcX1(a , b , delta));
            Console.WriteLine("x2 is: {0}", CalcX2(a , b , delta));
        }


        private static double CalcDelta(int a , int b , int c)
        {
            double r1 = Math.Pow(b, 2);
            double r2 = 4 * a * c;
            return r1 - r2;
        }
        private static double CalcX1(int a , int b , double delta)
        {
            double sq = Math.Sqrt(delta);
            double n = 0 - b + sq;
            double d = 2 * a;
            return n / d;
        }
        private static double CalcX2(int a , int b , double delta)
        {
            double sq = Math.Sqrt(delta);
            double n = 0 - b - sq;
            double d = 2 * a;
            return n / d;
        }
    }
}
