﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamrin2
{
    class Part5
    {
        public Part5()
        {
            Console.WriteLine("Part 5 ");
            Console.WriteLine("Enter account balance: ");
            int balance = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter percent : ");
            int percent = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter target : ");
            int target = int.Parse(Console.ReadLine());
            Console.WriteLine("After {0} years you reach the target", CalcTarget(balance , percent , target));

        }
        private static double CalcTarget(int balance , int percent , int target )
        {
            int r = balance;
            int year = 0;
            while(r < target)
            {
                year++;
                r = YearAmount(r, percent, year);
                Console.WriteLine("Balance after {0} years: {1}", year , r);
            }
            return year; 
        }
        private static int YearAmount(int balance , int percent , int year )
        {
            int s = percent * year;
            int profit = balance * s / 100;
            return balance + profit; 
        }
    }
}
