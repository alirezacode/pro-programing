﻿using System;

namespace Tamrin2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Tamrin 2");
            ShowMenu();
        }
        private static void ShowMenu()
        {
            Console.WriteLine("********************************************");
            Console.WriteLine("1- Part 1 (pow 2 & pow 3)");
            Console.WriteLine("2- Part 2 (x1 , x2)");
            Console.WriteLine("3- Part 3 (Age in days)");
            Console.WriteLine("4- Part 4 (GCD & LCM)");
            Console.WriteLine("5- Part 5 (Account balance)");
            Console.WriteLine("please enter part number or enter 0 to exit:");
            int part = Int32.Parse(Console.ReadLine());
            switch (part)
            {
                case 1:
                    new Part1();
                    break;
                case 2:
                    new Part2();
                    break;
                case 3:
                    new Part3();
                    break;
                case 4:
                    new Part4();
                    break;
                case 5:
                    new Part5();
                    break;
                case 0:
                default:
                    Environment.Exit(0);
                    break;
            }
            ShowMenu();
        }
    }
}
