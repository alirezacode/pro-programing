﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamrin2
{
    class Part1
    {
        public Part1()
        {
            Console.WriteLine("Part 1 ");
            Console.WriteLine("Please enter a number:");
            int num = Int32.Parse(Console.ReadLine());
            pow2(num);
            pow3(num);
        }

        private static void pow2(int num)
        {
            double res = Math.Pow(num, 2);
            Console.WriteLine("pow 2 is: {0}", res );
        }
        private static void pow3(int num)
        {
            double res = Math.Pow(num, 3);
            Console.WriteLine("pow 3 is: {0}", res );
        }
    }
}
